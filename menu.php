<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <title>Menu</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-woox-travel.css">
    <link rel="stylesheet" href="assets/css/owl.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet"href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>
<!--

TemplateMo 580 Woox Travel

https://templatemo.com/tm-580-woox-travel

-->
  </head>

<body>

<?php include("includes/ui/navbar.php"); ?>

  <div class="page-heading">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h4>Sesuai dengan budget dan kebutuhan Anda</h4>
          <h2>Pilih Menu Yang Anda Inginkan &amp; More</h2>
        </div>
      </div>
    </div>
  </div>

  <div class="amazing-deals">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 offset-lg-3">
          <div class="section-heading text-center">
            <h2>Pilih Menu Yang Anda Inginkan</h2>
            <p>Berdasar Fasilitas dan Harga yang tersedia</p>
          </div>
        </div>
        <div class="col-lg-6 col-sm-6">
          <div class="item">
            <div class="row">
              <div class="col-lg-6">
                <div class="image">
                  <img src="assets/images/deals-01.jpg" alt="">
                </div>
              </div>
              <div class="col-lg-6 align-self-center">
                <div class="content">
                  <h4>Honeymoon</h4>
                  <div class="row">
                    <div class="col-6">
                      <i class="fa fa-bed"></i>
                      <span class="list">Double Bed (Special)</span>
                    </div>
                    <div class="col-6">
                      <i class="fa fa-wifi"></i>
                      <span class="list">Free Wifi</span>
                    </div>
                    <div class="col-6">
                      <i class="fa fa-utensils"></i>
                      <span class="list">Free Breakfast dan Special Romantic Dinner</span>
                    </div>
                  </div>
                  <div class="main-button">
                    <a href="reservation.php">Buat Pesanan Sekarang</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-sm-6">
          <div class="item">
            <div class="row">
              <div class="col-lg-6">
                <div class="image">
                  <img src="assets/images/deals-02.jpg" alt="">
                </div>
              </div>
              <div class="col-lg-6 align-self-center">
                <div class="content">
                  <h4>Family</h4>
                  <div class="row">
                  <div class="col-6">
                      <i class="fa fa-bed"></i>
                      <span class="list">1 Single Bed (King Size) dan 1 Twin Bed</span>
                    </div>
                    <div class="col-6">
                      <i class="fa fa-wifi"></i>
                      <span class="list">Free Wifi</span>
                    </div>
                    <div class="col-6">
                      <i class="fa fa-utensils"></i>
                      <span class="list">Free Breakfast dan Dinner</span>
                    </div>
                  </div>
                  <div class="main-button">
                    <a href="reservation.php">Buat Pesanan Sekarang</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-sm-6">
          <div class="item">
            <div class="row">
              <div class="col-lg-6">
                <div class="image">
                  <img src="assets/images/deals-03.jpg" alt="">
                </div>
              </div>
              <div class="col-lg-6 align-self-center">
                <div class="content">
                  <h4>Single</h4>
                  <div class="row">
                  <div class="col-6">
                      <i class="fa fa-bed"></i>
                      <span class="list">Single Bed (Normal Size)</span>
                    </div>
                    <div class="col-6">
                      <i class="fa fa-wifi"></i>
                      <span class="list">Free Wifi</span>
                    </div>
                    <div class="col-6">
                      <i class="fa fa-utensils"></i>
                      <span class="list">Free Breakfast dan Dinner</span>
                    </div>
                  </div>
                  <div class="main-button">
                    <a href="reservation.php">Buat Pesanan Sekarang</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12">
          <ul class="page-numbers">
            <li><a href="#"><i class="fa fa-arrow-left"></i></a></li>
            <li><a href="#">1</a></li>
            <li class="active"><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#"><i class="fa fa-arrow-right"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <p>Copyright © 2022. 5RIKANDI Company.
            <br>
            All rights reserved.</p>
        </div>
      </div>
    </div>
  </footer>


  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

  <script src="assets/js/isotope.min.js"></script>
  <script src="assets/js/owl-carousel.js"></script>
  <script src="assets/js/wow.js"></script>
  <script src="assets/js/tabs.js"></script>
  <script src="assets/js/popup.js"></script>
  <script src="assets/js/custom.js"></script>

  <script>
    $(".option").click(function(){
      $(".option").removeClass("active");
      $(this).addClass("active"); 
    });
  </script>

  </body>

</html>
