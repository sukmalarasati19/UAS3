<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <title>About</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-woox-travel.css">
    <link rel="stylesheet" href="assets/css/owl.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet"href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>
<!--

TemplateMo 580 Woox Travel

https://templatemo.com/tm-580-woox-travel

-->
  </head>

<body>

<?php include("includes/ui/navbar.php"); ?>

  <!-- ***** Main Banner Area Start ***** -->
  <div class="about-main-content">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="content">
            <div class="blur-bg"></div>
            <h4>EXPLORE OUR RESORT</h4>
            <div class="line-dec"></div>
            <h2>Welcome To 5RIKANDI RESORT</h2>
            <p>Tempat resort yang nyaman dengan pemandangan yang menyenangkan</p>
            <div class="main-button">
              <a href="menu.php">Baca Selengkapnya</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ***** Main Banner Area End ***** -->
  
  <div class="cities-town">
    <div class="container">
      <div class="row">
        <div class="slider-content">
          <div class="row">
            <div class="col-lg-12">
              <h2>5RIKANDI'S <em>FACILITIES</em></h2>
            </div>
            <div class="col-lg-12">
              <div class="owl-cites-town owl-carousel">
                <div class="item">
                  <div class="thumb">
                    <img src="assets/images/parking-area.jpg" alt="">
                    <h4>Parking Area</h4>
                  </div>
                </div>
                <div class="item">
                  <div class="thumb">
                    <img src="assets/images/wifi.jfif" alt="">
                    <h4>Free Wifi</h4>
                  </div>
                </div>
                <div class="item">
                  <div class="thumb">
                    <img src="assets/images/mini-bar.jpg" alt="">
                    <h4>Mini Bar</h4>
                  </div>
                </div>
                <div class="item">
                  <div class="thumb">
                    <img src="assets/images/kafe-dan-restoran.jpg" alt="">
                    <h4>Cafe and Restaurant</h4>
                  </div>
                </div>
                <div class="item">
                  <div class="thumb">
                    <img src="assets/images/wahana-air.jpg" alt="">
                    <h4>Wahana Air</h4>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="weekly-offers">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 offset-lg-3">
          <div class="section-heading text-center">
            <h2>OUR ARTICLE</h2>
            <p>Cek berbagai diskon dan informasi menarik lainnya disini!</p>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="owl-weekly-offers owl-carousel">
            <div class="item">
              <div class="thumb">
                <img src="assets/images/about-us.jpg" alt="">
                <div class="text">
                <h4>ABOUT US</h4>
                  <div class="line-dec"></div>
                  <ul>
                    <li>About 5RIKANDI</li>
                    <li><i></i>5rikandi resort berdiri pada
                    <li><i></i>tahun 2017 yang terletak di</li>
                    <li><i></i>Pulau Jawa tepatnya Daerah...</li>
                  </ul>
                  <div class="main-button">
                    <a href="blog1.php">Lebih Lanjut</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="thumb">
                <img src="assets/images/ultah.jpg" alt="">
                <div class="text">
                <h4>PT. 5RIKANDI RESORT 
                  <br>  
                  RAYAKAN ULANG 
                  <br>
                  TAHUNNYA YANG KE-5</h4>
                  <div class="line-dec"></div>
                  <ul>
                    <li>ULTAH 5RIKANDI</li>
                    <li><i></i>5rikandi Resort perusahaan pengelola
                    <li><i></i>dan pengembang kawasan pariwisata</li>
                    <li><i></i>lokal di Gunung Kidul...</li>
                  </ul>
                  <div class="main-button">
                    <a href="blog2.php">Lebih Lanjut</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="thumb">
                <img src="assets/images/award.jpg" alt="">
                <div class="text">
                <h4>5RIKANDI RESORT 
                  <br>  
                  RAIH PENGHARGAAN 
                  <br>
                  DARI WORLD TRAVEL 
                  <br>
                  AWARDS 2022</h4>
                  <div class="line-dec"></div>
                  <ul>
                    <li>5RIKANDI's AWARD</li>
                    <li><i></i>DIY: Penghargaan disabet
                    <li><i></i>oleh 5rikandi Resort sebagai</li>
                    <li><i></i>Leading Resort oleh World...</li>
                  </ul>
                  <div class="main-button">
                    <a href="blog3.php">Lebih Lanjut</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="thumb">
                <img src="assets/images/diskon-01.jpg" alt="">
                <div class="text">
                <h4>WOW DISCOUNT 50% 
                  <br>  
                  SPECIAL CHRISTMAS AND 
                  <br>
                  NEW YEAR</h4>
                  <div class="line-dec"></div>
                  <ul>
                    <li>WOW DISCOUNT!</li>
                    <li><i></i>Mau liburan bareng keluarga?
                    <li><i></i>teman? pasangan? atau sendiri?</li>
                    <li><i></i>Bingung mau kemana?...</li>
                  </ul>
                  <div class="main-button">
                    <a href="blog4.php">Lebih Lanjut</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <p>Copyright © 2022. 5RIKANDI Company.
            <br>
            All rights reserved.</p>
        </div>
      </div>
    </div>
  </footer>


  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

  <script src="assets/js/isotope.min.js"></script>
  <script src="assets/js/owl-carousel.js"></script>
  <script src="assets/js/wow.js"></script>
  <script src="assets/js/tabs.js"></script>
  <script src="assets/js/popup.js"></script>
  <script src="assets/js/custom.js"></script>

  <script>
    $(".option").click(function(){
      $(".option").removeClass("active");
      $(this).addClass("active"); 
    });
  </script>

  </body>

</html>
