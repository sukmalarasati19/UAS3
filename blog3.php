<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>5RIKANDI'S BLOG</title>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-lig bg-dark navbar-dark text-lg-start">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.php"><img src="assets/images/logo2.png" alt="logo" width="60px"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                <a class="nav-link" aria-current="page" href="index.php">Home</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="about.php">About</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="blog.php">Blog</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="reservation.php">Reservation</a>
                </li>
            </ul>
            </div>
        </div>
        </nav> 
    </header>

    <main>
        <section class="bg-image" style="background-image: url">

        </section>
        <div class="container mt-5 mb-5">
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <div class="card border-light bg-light">
                        <div class="card-body">
                            <center>
                                <h1>5RIKANDI RESORT RAIH PENGHARGAAN DARI WORLD TRAVEL AWARDS 2022</h1>
                            </center>
                            <br>
                            <p align="justify">DIY: Penghargaan disabet oleh 5rikandi Resort sebagai Leading Resort oleh World Travel Awards pada tanggal 5 Desember 2022.  Resort yang berlokasi di Jl. Pantai Selatan Jawa, Tepus, Kab. Gunung Kidul, DI Yogyakarta dinobatkan sebagai salah satu resort bergengsi dan terkemuka. Ini kali pertama 5rikandi resort mendapatkan penghargaan tersebut
                            <br>
                            “Selamat dan Sukses untuk 5rikandi resort karena telah menerima penghargaan sebagai Leading Resort oleh World Travel Awards 2022,” tulis pernyataan 5rikandi resort pada akun instagram.
                            <br>
                            Acara penghargaan ini sudah berlangsung semenjak 8 tahun belakangan. Tujuannya untuk mengakui, memberikan penghargaan, dan merayakan keunggulan pada sektor pariwisata. Harapan kedepannya 5rikandi resort terus berkembang dan banyak diminati pengunjung baik wisatawan lokal hingga mancanegara. 

                            </p>
                        </div>
                    </div>       
                </div>
            </div>
        </div>   
    </main>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <footer class="page-footer text-white text-center text-lg-start bg-dark sticky-bottom">
      <div class="container p-5">
        <div class="row mt-4">
          <div class="col-lg-12 col-md-12 mb-4 mb-md-0">
            <h5 class="text-uppercase mb-4 text-center">About company</h5>
            <p align="justify">
            5rikandi resort berdiri pada tahun 2017 yang terletak di Pulau Jawa tepatnya Daerah Istimewa Yogyakarta. 5rikandi resort menyediakan 20 kamar.
            <br>
            Dengan akses penuh ke semua tempat makan dan rekreasi di 5rikandi Resort serta antar-jemput resor gratis. Terdapat 10 restoran dan bar pantai pasir putih terpencil; 12 kolam renang, lapangan golf dan pusat kebugaran dan Wi-Fi gratis. 
            </p>

            <div class="mt-4">
              <!-- Facebook -->
              <a src="https://id-id.facebook.com/login/device-based/regular/login/?login_attempt=1" type="button" class="btn btn-floating btn-light btn-lg"><i class="fab fa-facebook-f"></i></a>
              <!-- Twitter -->
              <a src="https://twitter.com/i/flow/login" type="button" class="btn btn-floating btn-light btn-lg"><i class="fab fa-twitter"></i></a>
              <!-- Google -->
              <a src="https://accounts.google.com/v3/signin/identifier?dsh=S-1777411593%3A1671701175174899&hl=id&flowName=GlifWebSignIn&flowEntry=ServiceLogin&ifkv=AeAAQh6M_WODF8MESjMnUa6gGsc3b9fpwD2g42WLZa4lcV0dhoPJfKY6ZrbeSQi2EQbLud_8dg8q" type="button" class="btn btn-floating btn-light btn-lg"><i class="fab fa-google-g"></i></a>
            </div>
          </div>
    
          <div class="text-center p-3">
              <div class="row">
                <div class="col-lg-12">
                  <p>Copyright © 2022. 5RIKANDI Company.
                    <br>
                    All rights reserved.</p>
                </div>
              </div>
          </div>

    </footer>

</body>
</html>