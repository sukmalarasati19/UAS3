<?php 
include_once("includes/item.php");
include_once("includes/kategori.php");
include_once("includes/pemesanan.php");

if (isset($_POST["kategori"]) && isset($_POST["tamu"]) && isset($_POST["tgl"])) {
    $items = items_filter_by_pemesanan($_POST["kategori"], $_POST["tamu"], $_POST["tgl"]);
    header("Content-Type: application/json");
    echo json_encode($items);
}
?>