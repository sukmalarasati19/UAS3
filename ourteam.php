<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="assets/css/styleourteam.css">
  <title>Our Team</title>
</head>
<body>

  <div class="person" style="--color: #3e6e7c">
    <div class="container">
      <div class="conianer-inner">
        <div class="circle"></div>
        <img src="assets/images/image1.png">
      </div>
    </div>
    <div class="divider"></div>
    <center><h3>Annisa Nur Chasidiyah</h3></center>
    <p>K3521010</p>
  </div>

  <div class="person" style="--color: #37375e">
    <div class="container">
      <div class="conianer-inner">
        <div class="circle"></div>
        <img src="assets/images/image2.png">
      </div>
    </div>
    <div class="divider"></div>
    <center><h3>Gabriella Caroline P.N</h3></center>
    <p>K3521030</p>
  </div>

  <div class="person" style="--color: #3e6e7c">
    <div class="container">
      <div class="conianer-inner">
        <div class="circle"></div>
        <img src="assets/images/image3.png">
      </div>
    </div>
    <div class="divider"></div>
    <center><h3>Kharisma Rosyiana Putri</h3></center>
    <p>K3521038</p>
  </div>

  <div class="person" style="--color: #37375e">
    <div class="container">
      <div class="conianer-inner">
        <div class="circle"></div>
        <img src="assets/images/image4.png">
      </div>
    </div>
    <div class="divider"></div>
    <center><h3>Maharani Ladja Puwa</h3></center>
    <p>K3521078</p>
  </div>

  <div class="person" style="--color: #3e6e7c">
    <div class="container">
      <div class="conianer-inner">
        <div class="circle"></div>
        <img src="assets/images/image5.png">
      </div>
    </div>
    <div class="divider"></div>
    <center><h3>Sukma Larasati</h3></center>
    <p>K3521066</p>
  </div>

</body>
</html>