<?php
include_once("koneksi.php");

function kategori_create($nama, $kapasitas_min, $kapasitas_max){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("INSERT INTO kategori (nama, `kapasitas_min`, `kapasitas_max`) VALUES (?,?,?)");

    $q->bind_param("sii", $nama, $kapasitas_min, $kapasitas_max);
    return $q->execute();
}

function kategori_delete($id){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("DELETE FROM kategori where id=?");
    $q->bind_param("i", $id);
    return $q->execute();
}

function kategori_get($id){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("SELECT kategori.id as id, kategori.nama as nama, kategori.kapasitas_min as `kapasitas_min`, kategori.kapasitas_max as `kapasitas_max` FROM kategori where kategori.id=? LIMIT 1");
    $q->bind_param("i", $id);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        if (count($res) == 0)
            return false;
        return $res[0];
    }
    else
        return false;
}

function kategori($limit, $offset){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("SELECT kategori.id as id, kategori.nama as nama, kategori.kapasitas_min as `kapasitas_min`, kategori.kapasitas_max as `kapasitas_max` FROM kategori LIMIT ? OFFSET ?");
    $q->bind_param("ii", $limit, $offset);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);

        return $res;
    }
    else
        return array();
}