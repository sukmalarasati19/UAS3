<?php
include_once("koneksi.php");

function area_create($nama, $gambar, $deskripsi){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("INSERT INTO area (nama, gambar, deskripsi) VALUES (?,?,?)");

    $q->bind_param("sis", $nama, $gambar, $deskripsi);
    return $q->execute();
}

function area_delete($id){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("DELETE FROM area where id=?");
    $q->bind_param("i", $id);
    return $q->execute();
}

function area_get($id){
    $SQL = $GLOBALS["SQL"];
    $url_base = $GLOBALS["url_base"];
    $q = $SQL->prepare("SELECT area.id as id, area.nama as nama, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, area.deskripsi as deskripsi FROM area join galeri where galeri.id=area.gambar and area.id=? LIMIT 1");
    $q->bind_param("i", $id);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        if (count($res) == 0)
            return false;
        $url = $url_base . "/galeri/" . $res[0]["galeri_lokasi"];
        $res[0]["gambar"] = $url;
        return $res[0];
    }
    else
        return false;
}

function area($limit, $offset){
    $SQL = $GLOBALS["SQL"];
    $url_base = $GLOBALS["url_base"];
    $q = $SQL->prepare("SELECT area.id as id, area.nama as nama, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, area.deskripsi as deskripsi FROM area join galeri where galeri.id=area.gambar LIMIT ? OFFSET ?");
    $q->bind_param("ii", $limit, $offset);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        $ret = array();
        foreach ($res as $row) {
            $url = $url_base . "/galeri/" . $row["galeri_lokasi"];
            $row["gambar"] = $url;
            $ret[] = $row;
        }

        return $ret;
    }
    else
        return array();
}