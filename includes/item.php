<?php
include_once("koneksi.php");

function item_create($unit, $area, $kapasitas, $harga, $gambar, $kategori){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("INSERT INTO item (unit, area, kapasitas, harga, gambar, kategori) VALUES (?,?,?,?,?,?)");

    $q->bind_param("siiiii", $unit, $area, $kapasitas, $harga, $gambar, $kategori);
    return $q->execute();
}

function item_update($id, $unit, $area, $kapasitas, $harga, $gambar, $kategori){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("UPDATE item SET unit=?, area=?, kapasitas=?, harga=?, gambar=?, kategori=? WHERE id=?");

    $q->bind_param("siiiiii", $unit, $area, $kapasitas, $harga, $gambar, $kategori, $id);
    return $q->execute();
}

function item_delete($id){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("DELETE FROM item where id=?");
    $q->bind_param("i", $id);
    return $q->execute();
}

function item_get($id){
    $SQL = $GLOBALS["SQL"];
    $url_base = $GLOBALS["url_base"];
    $q = $SQL->prepare("SELECT item.id as id, item.unit as unit, area.id as area_id, area.nama as area_nama, area.galeri_id as area_galeri_id, area.galeri_lokasi as area_galeri_lokasi, area.deskripsi as area_deskripsi, item.kapasitas as kapasitas, item.harga as harga, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, kategori.id as kategori_id, kategori.nama as kategori_nama, kategori.kapasitas_min as kategori_kapasitas_min, kategori.kapasitas_max as kategori_kapasitas_max FROM item, (SELECT area.id as id, area.nama as nama, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, area.deskripsi as deskripsi FROM area join galeri where galeri.id=area.gambar) as area, kategori, galeri WHERE area.id = item.area and galeri.id = item.gambar and kategori.id = item.kategori and item.id=? ORDER BY item.id LIMIT 1");
    $q->bind_param("i", $id);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        if (count($res) == 0)
            return false;
        $url = $url_base . "/galeri/" . $res[0]["area_galeri_lokasi"];
        $res[0]["area_gambar"] = $url;
        $url = $url_base . "/galeri/" . $res[0]["galeri_lokasi"];
        $res[0]["gambar"] = $url;
        return $res[0];
    }
    else
        return false;
}

function item($limit, $offset){
    $SQL = $GLOBALS["SQL"];
    $url_base = $GLOBALS["url_base"];
    $q = $SQL->prepare("SELECT item.id as id, item.unit as unit, area.id as area_id, area.nama as area_nama, area.galeri_id as area_galeri_id, area.galeri_lokasi as area_galeri_lokasi, area.deskripsi as area_deskripsi, item.kapasitas as kapasitas, item.harga as harga, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, kategori.id as kategori_id, kategori.nama as kategori_nama, kategori.kapasitas_min as kategori_kapasitas_min, kategori.kapasitas_max as kategori_kapasitas_max FROM item, (SELECT area.id as id, area.nama as nama, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, area.deskripsi as deskripsi FROM area join galeri where galeri.id=area.gambar) as area, kategori, galeri WHERE area.id = item.area and galeri.id = item.gambar and kategori.id = item.kategori ORDER BY item.id LIMIT ? OFFSET ?");
    $q->bind_param("ii", $limit, $offset);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        $ret = array();
        foreach ($res as $row) {
            $url = $url_base . "/galeri/" . $row["area_galeri_lokasi"];
            $row["area_gambar"] = $url;
            $url = $url_base . "/galeri/" . $row["galeri_lokasi"];
            $row["gambar"] = $url;
            $ret[] = $row;
        }

        return $ret;
    }
    else
        return array();
}

function item_by_area($area, $limit, $offset){
    $SQL = $GLOBALS["SQL"];
    $url_base = $GLOBALS["url_base"];
    $q = $SQL->prepare("SELECT item.id as id, item.unit as unit, area.id as area_id, area.nama as area_nama, area.galeri_id as area_galeri_id, area.galeri_lokasi as area_galeri_lokasi, area.deskripsi as area_deskripsi, item.kapasitas as kapasitas, item.harga as harga, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, kategori.id as kategori_id, kategori.nama as kategori_nama, kategori.kapasitas_min as kategori_kapasitas_min, kategori.kapasitas_max as kategori_kapasitas_max FROM item, (SELECT area.id as id, area.nama as nama, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, area.deskripsi as deskripsi FROM area join galeri where galeri.id=area.gambar and area.id=?) as area, kategori, galeri WHERE area.id = item.area and galeri.id = item.gambar and kategori.id = item.kategori ORDER BY item.id LIMIT ? OFFSET ?");
    $q->bind_param("iii", $area, $limit, $offset);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        $ret = array();
        foreach ($res as $row) {
            $url = $url_base . "/galeri/" . $row["area_galeri_lokasi"];
            $row["area_gambar"] = $url;
            $url = $url_base . "/galeri/" . $row["galeri_lokasi"];
            $row["gambar"] = $url;
            $ret[] = $row;
        }

        return $ret;
    }
    else
        return array();
}

function item_by_kategori($kategori, $limit, $offset){
    $SQL = $GLOBALS["SQL"];
    $url_base = $GLOBALS["url_base"];
    $q = $SQL->prepare("SELECT item.id as id, item.unit as unit, area.id as area_id, area.nama as area_nama, area.galeri_id as area_galeri_id, area.galeri_lokasi as area_galeri_lokasi, area.deskripsi as area_deskripsi, item.kapasitas as kapasitas, item.harga as harga, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, kategori.id as kategori_id, kategori.nama as kategori_nama, kategori.kapasitas_min as kategori_kapasitas_min, kategori.kapasitas_max as kategori_kapasitas_max FROM item, (SELECT area.id as id, area.nama as nama, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, area.deskripsi as deskripsi FROM area join galeri where galeri.id=area.gambar) as area, kategori, galeri WHERE area.id = item.area and galeri.id = item.gambar and kategori.id = item.kategori and kategori.id=? ORDER BY item.id LIMIT ? OFFSET ?");
    $q->bind_param("iii", $kategori, $limit, $offset);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        $ret = array();
        foreach ($res as $row) {
            $url = $url_base . "/galeri/" . $row["area_galeri_lokasi"];
            $row["area_gambar"] = $url;
            $url = $url_base . "/galeri/" . $row["galeri_lokasi"];
            $row["gambar"] = $url;
            $ret[] = $row;
        }

        return $ret;
    }
    else
        return array();
}

function items(){
    $SQL = $GLOBALS["SQL"];
    $url_base = $GLOBALS["url_base"];
    $q = $SQL->prepare("SELECT item.id as id, item.unit as unit, area.id as area_id, area.nama as area_nama, area.galeri_id as area_galeri_id, area.galeri_lokasi as area_galeri_lokasi, area.deskripsi as area_deskripsi, item.kapasitas as kapasitas, item.harga as harga, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, kategori.id as kategori_id, kategori.nama as kategori_nama, kategori.kapasitas_min as kategori_kapasitas_min, kategori.kapasitas_max as kategori_kapasitas_max FROM item, (SELECT area.id as id, area.nama as nama, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, area.deskripsi as deskripsi FROM area join galeri where galeri.id=area.gambar) as area, kategori, galeri WHERE area.id = item.area and galeri.id = item.gambar and kategori.id = item.kategori ORDER BY item.id");
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        $ret = array();
        foreach ($res as $row) {
            $url = $url_base . "/galeri/" . $row["area_galeri_lokasi"];
            $row["area_gambar"] = $url;
            $url = $url_base . "/galeri/" . $row["galeri_lokasi"];
            $row["gambar"] = $url;
            $ret[] = $row;
        }

        return $ret;
    }
    else
        return array();
}

function item_fasilitas(){
    $items = items();
    $kap_min = array();
    $kap_max = array();
    $count = array();
    $harga = array();
    $kategori = array();
    foreach($items as $item){
        $kategori_id = $item["kategori_id"];
        $kap = (int)($item["kapasitas"]);
        $kat = $item["kategori_nama"];
        $hr = $item["harga"];

        if(!isset($kap_min[$kategori_id])) $kap_min[$kategori_id] = 0xFFFFFFFF;
        if($kap < $kap_min[$kategori_id]) $kap_min[$kategori_id] = $kap;

        if(!isset($kap_max[$kategori_id])) $kap_max[$kategori_id] = 1;
        if($kap > $kap_max[$kategori_id]) $kap_max[$kategori_id] = $kap;

        if(!isset($count[$kategori_id])) $count[$kategori_id] = 0;
        if(!isset($harga[$kategori_id])) $harga[$kategori_id] = 0;
        $harga[$kategori_id] += $hr;
        $count[$kategori_id] += 1;
        $kategori[$kategori_id] = $kat;
    }

    $ret = array();

    foreach($count as $i => $c){
        $arr = array();
        $arr["id"] = $i;
        $arr["min"] = $kap_min[$i];
        $arr["max"] = $kap_max[$i];

        if ($arr["min"] == $arr["max"])
            $arr["kapasitas"] = $arr["min"];
        else
            $arr["kapasitas"] = $arr["min"] . " - " . $arr["max"];

        $hr = $harga[$i];
        $arr["harga_rata_rata"] = $hr / $c;

        $arr["kategori"] = $kategori[$i];

        $ret[] = $arr;
    }

    return $ret;
}

// SELECT item.id as id, item.unit as unit, area.id as area_id, area.nama as area_nama, area.galeri_id as area_galeri_id, area.galeri_lokasi as area_galeri_lokasi, area.deskripsi as area_deskripsi, item.kapasitas as kapasitas, item.harga as harga, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, kategori.id as kategori_id, kategori.nama as kategori_nama, kategori.kapasitas_min as kategori_kapasitas_min, kategori.kapasitas_max as kategori_kapasitas_max FROM item, (SELECT area.id as id, area.nama as nama, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, area.deskripsi as deskripsi FROM area join galeri where galeri.id=area.gambar) as area, kategori, galeri WHERE area.id = item.area and galeri.id = item.gambar and kategori.id = item.kategori and item.id not in (SELECT pemesanan.item as item FROM pemesanan) ORDER BY item.id;

function items_filter_by_pemesanan($kategori, $kapasitas, $tgl){
    $SQL = $GLOBALS["SQL"];
    $url_base = $GLOBALS["url_base"];
    $q = $SQL->prepare("SELECT item.id as id, item.unit as unit, area.id as area_id, area.nama as area_nama, area.galeri_id as area_galeri_id, area.galeri_lokasi as area_galeri_lokasi, area.deskripsi as area_deskripsi, item.kapasitas as kapasitas, item.harga as harga, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, kategori.id as kategori_id, kategori.nama as kategori_nama, kategori.kapasitas_min as kategori_kapasitas_min, kategori.kapasitas_max as kategori_kapasitas_max FROM item, (SELECT area.id as id, area.nama as nama, galeri.id as galeri_id, galeri.lokasi as galeri_lokasi, area.deskripsi as deskripsi FROM area join galeri where galeri.id=area.gambar) as area, kategori, galeri WHERE area.id = item.area and galeri.id = item.gambar and kategori.id = item.kategori and item.kategori=? and item.kapasitas=? and item.id not in (SELECT pemesanan.item as item FROM pemesanan WHERE pemesanan.status = 2 and pemesanan.status = 3 and pemesanan.tanggal=?) ORDER BY item.id");
    $q->bind_param("iis", $kategori, $kapasitas, $tgl);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        $ret = array();
        foreach ($res as $row) {
            $url = $url_base . "/galeri/" . $row["area_galeri_lokasi"];
            $row["area_gambar"] = $url;
            $url = $url_base . "/galeri/" . $row["galeri_lokasi"];
            $row["gambar"] = $url;
            $ret[] = $row;
        }

        return $ret;
    }
    else
        return array();
}