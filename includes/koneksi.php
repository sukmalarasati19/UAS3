<?php
include_once("config.php");

if($mysql_debug)
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

$SQL = new mysqli($mysql_host, $mysql_user, $mysql_passwd, $mysql_db, $mysql_port);