<?php
include_once("koneksi.php");

$mime_type = array(
    "image/jpeg" => "jpg",
    "image/png" => "png",
);

function create_name($file){
    return md5_file($file["tmp_name"]);
}

function galeri_create_by_file($file){
    $mime_type = $GLOBALS["mime_type"];
    $f = create_name($file) . "." . $mime_type[$file["type"]];
    $fn =  __DIR__ . "/../galeri/" . $f;
    move_uploaded_file($file["tmp_name"], $fn);

    return galeri_create_by_path($f);
}

function galeri_create_by_path($path){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("INSERT INTO galeri (lokasi) VALUES (?)");

    $q->bind_param("s", $path);
    return $q->execute();
}

function galeri_delete($id){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("DELETE FROM galeri where id=?");
    $q->bind_param("i", $id);
    return $q->execute();
}

function galeri_get($id){
    $SQL = $GLOBALS["SQL"];
    $url_base = $GLOBALS["url_base"];
    $q = $SQL->prepare("SELECT * FROM galeri where id=? LIMIT 1");
    $q->bind_param("i", $id);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        $id = $res[0]["id"];
        $url = $url_base . "/galeri/" . $res[0]["lokasi"];
        return array(
            "id" => $id,
            "url" => $url
        );
    }
    else
        return array();
}

function galeri($limit, $offset){
    $SQL = $GLOBALS["SQL"];
    $url_base = $GLOBALS["url_base"];
    $q = $SQL->prepare("SELECT * FROM galeri LIMIT ? OFFSET ?");
    $q->bind_param("ii", $limit, $offset);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        $ret = array();
        foreach ($res as $row) {
            $id = $row["id"];
            $url = $url_base . "/galeri/" . $row["lokasi"];
            $ret[] = array(
                    "id" => $id,
                    "url" => $url
                );
        }

        return $ret;
    }
    else
        return array();
}