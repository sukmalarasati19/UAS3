<?php
include_once("koneksi.php");

function admin_create($nama, $password){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("INSERT INTO admin (nama, password) VALUES (?,MD5(?))");

    $q->bind_param("ss", $nama, $password);
    return $q->execute();
}

function admin_verifikasi($nama, $password){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("SELECT admin.id as id, admin.nama as nama FROM admin WHERE admin.nama=? and admin.password=MD5(?) LIMIT 1");
    $q->bind_param("ss", $nama, $password);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        if (count($res) == 0)
            return false;

        return $res[0];
    }
    else
        return false;
}

function admin_delete($id){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("DELETE FROM admin where id=?");
    $q->bind_param("i", $id);
    return $q->execute();
}

function admin_get($id){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("SELECT admin.id as id, admin.nama as nama FROM admin WHERE admin.id=? LIMIT 1");
    $q->bind_param("i", $id);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        return $res[0];
    }
    else
        return false;
}

function admin($limit, $offset){
    $SQL = $GLOBALS["SQL"];
    $url_base = $GLOBALS["url_base"];
    $q = $SQL->prepare("SELECT admin.id as id, admin.nama as nama FROM admin LIMIT ? OFFSET ?");
    $q->bind_param("ii", $limit, $offset);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);

        return $res;
    }
    else
        return array();
}