<?php
session_start();
$admin_id = 0;
if (isset($_SESSION["admin_id"]))
    $admin_id = $_SESSION["admin_id"];
else {
    header("Location: login.php");
    exit(0);
}