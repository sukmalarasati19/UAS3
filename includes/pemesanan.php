<?php
include_once("koneksi.php");
include_once("item.php");

function pemesanan_create($no_pemesanan, $pemesan_nama, $pemesan_email, $pemesan_notelp, $item, $tanggal){
    $status = 1;
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("INSERT INTO pemesanan (no_pemesanan, pemesan_nama, pemesan_email, pemesan_notelp, item, tanggal, status) VALUES (?,?,?,?,?,?,?)");

    $q->bind_param("isssisi", $no_pemesanan, $pemesan_nama, $pemesan_email, $pemesan_notelp, $item, $tanggal, $status);

    if ($q->execute())
        return $q->id;
    else
        return false;
}

function pemesanan_delete($id){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("DELETE FROM pemesanan where id=?");
    $q->bind_param("i", $id);
    return $q->execute();
}

function pemesanan_get($id){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("SELECT pemesanan.no_pemesanan as no_pemesanan, pemesanan.pemesan_nama as pemesan_nama, pemesanan.pemesan_email as pemesan_email, pemesanan.pemesan_notelp as pemesan_notelp, pemesanan.item as item, pemesanan.tanggal as tanggal, status.id as status_id, status.nama as status_nama FROM pemesanan, status where pemesanan.status = status.id and pemesanan.id=? LIMIT 1");
    $q->bind_param("i", $id);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        if (count($res) == 0)
            return false;
        $res[0]["item"] = item_get($res[0]["item"]);
        return $res[0];
    }
    else
        return false;
}

function pemesanan_get_by_no($no){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("SELECT pemesanan.no_pemesanan as no_pemesanan, pemesanan.pemesan_nama as pemesan_nama, pemesanan.pemesan_email as pemesan_email, pemesanan.pemesan_notelp as pemesan_notelp, pemesanan.item as item, pemesanan.tanggal as tanggal, status.id as status_id, status.nama as status_nama FROM pemesanan, status where pemesanan.status = status.id and pemesanan.no_pemesanan=? LIMIT 1");
    $q->bind_param("i", $no);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);
        if (count($res) == 0)
            return false;
        $res[0]["item"] = item_get($res[0]["item"]);
        return $res[0];
    }
    else
        return false;
}

function pemesanan($limit, $offset){
    $SQL = $GLOBALS["SQL"];
    $q = $SQL->prepare("SELECT pemesanan.no_pemesanan as no_pemesanan, pemesanan.pemesan_nama as pemesan_nama, pemesanan.pemesan_email as pemesan_email, pemesanan.pemesan_notelp as pemesan_notelp, pemesanan.item as item, pemesanan.tanggal as tanggal, status.id as status_id, status.nama as status_nama FROM pemesanan, status where pemesanan.status = status.id ORDER BY pemesanan.id LIMIT ? OFFSET ?");
    $q->bind_param("ii", $limit, $offset);
    if ($q->execute()) {
        $res = $q->get_result()->fetch_all(MYSQLI_ASSOC);

        for($i = 0; $i < count($res); $i += 1){
            $res[$i]["item"] = item_get($res[$i]["item"]);
        }

        return $res;
    }
    else
        return array();
}