<?php 
$page = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
function active($p){
    if($GLOBALS["page"] == $p) echo "class=\"active\"";
}

session_start();
function check_pemesanan(){
    if(isset($_SESSION["pemesanan"])){
        if(count($_SESSION["pemesanan"]) > 0){
            return count($_SESSION["pemesanan"]);
        }
    }

    return false;
}
?>
<!-- ***** Preloader Start ***** -->
<div id="js-preloader" class="js-preloader">
    <div class="preloader-inner">
        <span class="dot"></span>
        <div class="dots">
        <span></span>
        <span></span>
        <span></span>
        </div>
    </div>
</div>
<!-- ***** Preloader End ***** -->

<!-- ***** Header Area Start ***** -->
<header class="header-area header-sticky">
<div class="container">
    <div class="row">
        <div class="col-12">
            <nav class="main-nav">
                <!-- ***** Logo Start ***** -->
                <a href="index.php" class="logo">
                    <img src="assets/images/logo2.png" alt="">
                </a>
                <!-- ***** Logo End ***** -->
                <!-- ***** Menu Start ***** -->
                <ul class="nav">
                    <li><a href="index.php" <?php active("index.php"); ?>>Home</a></li>
                    <li><a href="about.php" <?php active("about.php"); ?>>About</a></li>
                    <li><a href="menu.php" <?php active("menu.php"); ?>>Menu</a></li>
                    <li><a href="reservation.php" <?php active("reservation.php"); ?>>Reservation</a></li>
                    <?php if(check_pemesanan()): ?>
                    <li><a style="color:yellow;" href="pemesanan.php" <?php active("pemesanan.php"); ?>>Pemesanan (<?php echo check_pemesanan(); ?>)</a></li>
                    <?php endif; ?>
                </ul>   
                <a class='menu-trigger'>
                    <span>Menu</span>
                </a>
                <!-- ***** Menu End ***** -->
            </nav>
        </div>
    </div>
</div>
</header>
<!-- ***** Header Area End ***** -->