<?php 
include_once("includes/item.php");
$fasilitas = item_fasilitas();
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <title>5RIKANDI RESORT</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-woox-travel.css">
    <link rel="stylesheet" href="assets/css/owl.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet"href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>
    <link href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" rel="stylesheet"/>
<!--

TemplateMo 580 Woox Travel

https://templatemo.com/tm-580-woox-travel

-->

  </head>

<body>
  <?php include("includes/ui/navbar.php"); ?>
  <!-- ***** Main Banner Area Start ***** -->
  <section id="section-1">
    <div class="content-slider">
      
      <?php for($i = 1; $i <= count($fasilitas); $i += 1): ?>
      <input type="radio" id="banner<?php echo $i; ?>" class="sec-1-input" name="banner" <?php if($i == 1) echo "checked"; ?>>
      <?php endfor; ?>

      <div class="slider">
        <?php for($i = 1; $i <= count($fasilitas); $i += 1): ?>
        <?php $f = $fasilitas[$i - 1]; ?>
        <div id="top-banner-<?php echo $i; ?>" class="banner">
          <div class="banner-inner-wrapper header-text" style="background-color: rgba(0,0,0,.7);">
            <div class="main-caption">
              <h2>Nikmati waktu liburan menginap dengan paket:</h2>
              <h1><?php  echo $f["kategori"]; ?></h1>
              <div class="border-button"><a href="about.php">Kunjungi Sekarang</a></div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-lg-12">
                  <div class="more-info">
                    <div class="row">
                      <div class="col-lg-3 col-sm-6 col-6">
                        <div class="row">
                          <div class="col-4">
                            <i class="fa fa-user"></i>
                          </div>
                          <div class="col-8">
                            <h4><span>Maks Orang:</span><br><?php echo $f["kapasitas"]; ?></h4>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3 col-sm-6 col-6">
                        <div class="row">
                          <div class="col-4">
                            <i class="fa fa-wifi"></i>
                          </div>
                          <div class="col-8">
                            <h4><span>Fasilitas:</span><br>Free Wifi</h4>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3 col-sm-6 col-6">
                        <div class="row">
                          <div class="col-4">
                            <i class="fa fa-tag"></i>
                          </div>
                          <div class="col-8">
                            <h4><span>Harga Rata-rata:</span><br>Rp<?php echo number_format($f["harga_rata_rata"],2,',','.'); ?></h4>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3 col-sm-6 col-6">
                        <div class="main-button">
                          <a href="reservation.php">Lebih Lanjut</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php endfor; ?>
        
      </div>
      <nav>
        <div class="controls">
          <?php for($i = 1; $i <= count($fasilitas); $i += 1): ?>
          <label for="banner<?php echo $i; ?>"><span class="progressbar"><span class="progressbar-fill"></span></span><span class="text"><?php echo $i; ?></span></label>
          <?php endfor; ?>
        </div>
      </nav>
    </div>
  </section>
  <!-- ***** Main Banner Area End ***** -->

  <br><br><br>

  <img src="assets/images/diskon.png" alt="diskon">
  
  <div class="visit-country">
    <div class="container">
      <div class="row">
        <div class="col-lg-5">
          <div class="section-heading">
            <h2>Nikmati Sensasi Menginap Di Pinggir Pantai</h2>
            <p>Dengan pemandangan dan tempat menginap yang nyaman</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="items">
            <div class="row">
              <?php foreach($fasilitas as $f): ?>
              <?php $item = item_by_kategori($f["id"], 1, 0)[0]; ?>
              <div class="col-lg-12">
                <div class="item">
                  <div class="row">
                    <div class="col-lg-4 col-sm-5">
                      <div class="image">
                        <img src="<?php echo $item["gambar"]; ?>" alt="">
                      </div>
                    </div>
                    <div class="col-lg-8 col-sm-7">
                      <div class="right-content">
                        <h4><?php echo $item["area_nama"]; ?></h4>
                        <span><?php  echo $f["kategori"]; ?></span>
                        <div class="main-button">
                          <a href="reservation.php">Lebih Lanjut</a>
                        </div>
                        <p><?php echo $item["area_deskripsi"]; ?></p>
                        <ul class="info">
                          <li><i class="fa fa-user"></i> <?php echo $f["kapasitas"] ?> People</li>
                          <li></li>
                          <li><i class="fa fa-tag"></i> Rp<?php echo number_format($f["harga_rata_rata"],2,',','.'); ?></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php endforeach; ?>
              
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="side-bar-map">
            <div class="row">
              <div class="col-lg-12">
                <div id="map">
                    <div id="osm-map" style="width: 100%; height: 550px; background: #aad3df; border-radius: 23px; z-index: 0;"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <p>Copyright © 2022. 5RIKANDI Company.
            <br>
            All rights reserved.</p>
        </div>
      </div>
    </div>
  </footer>


  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

  <script src="assets/js/isotope.min.js"></script>
  <script src="assets/js/owl-carousel.js"></script>
  <script src="assets/js/wow.js"></script>
  <script src="assets/js/tabs.js"></script>
  <script src="assets/js/popup.js"></script>
  <script src="assets/js/custom.js"></script>
  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>

  <script>
    (() => {
      var element = document.getElementById('osm-map');
      var map = L.map(element);
      L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
          attribution: 'Srikandi Resort'
      }).addTo(map);
      var target = L.latLng('-8.1528202', '110.6158277');
      map.setView(target, 12);
      L.marker(target).on("click", (e) => {
        open("https://www.google.co.id/maps/place/Pantai,+Tepus,+Kec.+Tepus,+Kabupaten+Gunung+Kidul,+Daerah+Istimewa+Yogyakarta/@-8.1528202,110.6158277,14z/data=!3m1!4b1!4m5!3m4!1s0x2e7bb99df2a786cd:0xac232c33c942e20c!8m2!3d-8.1561723!4d110.6317907", "_blank")
      }).addTo(map);
    })()
  </script>

  <script>
    function bannerSwitcher() {
      next = $('.sec-1-input').filter(':checked').next('.sec-1-input');
      if (next.length) next.prop('checked', true);
      else $('.sec-1-input').first().prop('checked', true);
    }

    var bannerTimer = setInterval(bannerSwitcher, 5000);

    $('nav .controls label').click(function() {
      clearInterval(bannerTimer);
      bannerTimer = setInterval(bannerSwitcher, 5000)
    });
  </script>

  </body>

</html>
