<!DOCTYPE html>
<?php
include_once("includes/item.php");
include_once("includes/kategori.php");
include_once("includes/pemesanan.php");

session_start();

if(isset($_POST["nama"]) && isset($_POST["notelp"]) && isset($_POST["email"]) && isset($_POST["kategori"]) && isset($_POST["tamu"]) && isset($_POST["tgl"]) && isset($_POST["unit"])){
  $no = time();
  $id = pemesanan_create($no, $_POST["nama"], $_POST["email"], $_POST["notelp"], $_POST["unit"], $_POST["tgl"]);
  if($id){
    if(!isset($_SESSION["pemesanan"])) $_SESSION["pemesanan"] = array();
    $_SESSION["pemesanan"][] = $no;
  }
}

$kategori = kategori(100, 0);
?>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <title>Reservation</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-woox-travel.css">
    <link rel="stylesheet" href="assets/css/owl.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet"href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>
    <link href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" rel="stylesheet"/>
    <link href="vendor/select2/select2.min.css" rel="stylesheet" />
<!--

TemplateMo 580 Woox Travel

https://templatemo.com/tm-580-woox-travel

-->
  </head>

<body>

<?php include("includes/ui/navbar.php"); ?>

  <div class="second-page-heading">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h4>5RIKANDI RESORT</h4>
          <h2>Make Your Reservation</h2>
          <p>Tidak usah ragu tidak usah bimbang. Nikmati liburan yang menyenangkan di depan mata.
          <p>Ayo pesan sekarang !</p>
        </div>
      </div>
    </div>
  </div>

  <div class="more-info reservation-info">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-sm-6">
          <div class="info-item">
            <i class="fa fa-phone"></i>
            <h4>Telepon</h4>
            <a href="#">+62 888 999 123</a>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6">
          <div class="info-item">
            <i class="fa fa-envelope"></i>
            <h4>Alamat Email</h4>
            <a href="5rikandiptik@gmail.com">5rikandiptik@gmail.com</a>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6">
          <div class="info-item">
            <i class="fa fa-map-marker"></i>
            <h4>Alamat Kantor</h4>
            <a href="https://www.google.co.id/maps/place/Pantai,+Tepus,+Kec.+Tepus,+Kabupaten+Gunung+Kidul,+Daerah+Istimewa+Yogyakarta/@-8.1528202,110.6158277,14z/data=!3m1!4b1!4m5!3m4!1s0x2e7bb99df2a786cd:0xac232c33c942e20c!8m2!3d-8.1561723!4d110.6317907">SRIKANDI RESORT</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="reservation-form">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div id="map">
            <!-- <iframe src="https://www.google.co.id/maps/place/Pantai,+Tepus,+Kec.+Tepus,+Kabupaten+Gunung+Kidul,+Daerah+Istimewa+Yogyakarta/@-8.1528202,110.6158277,14z/data=!3m1!4b1!4m5!3m4!1s0x2e7bb99df2a786cd:0xac232c33c942e20c!8m2!3d-8.1561723!4d110.6317907" width="100%" height="450px" frameborder="0" style="border:0; border-top-left-radius: 23px; border-top-right-radius: 23px;" allowfullscreen=""></iframe> -->
                    
            <div id="osm-map" style="width: 100%; height: 450px; background: #aad3df; border-top-left-radius: 23px; border-top-right-radius: 23px; z-index: 0;"></div>
          </div>
        </div>
        <div class="col-lg-12">
          <form id="reservation-form" name="gs" method="POST" role="search">
            <div class="row">
              <div class="col-lg-12">
                <h4>Buat <em>Reservasimu</em> Lewat Formulir <em>Di Bawah Ini</em></h4>
              </div>
              <div class="col-lg-12">
                  <fieldset>
                      <label for="nama" class="form-label">Nama Lengkap</label>
                      <input type="text" name="nama" class="nama" placeholder="Nama Lengkap" required>
                  </fieldset>
              </div>
              <div class="col-lg-6">
                <fieldset>
                    <label for="notelp" class="form-label">Nomor Telepon</label>
                    <input type="phone" name="notelp" class="notelp" placeholder="+62xxxxxxxxxxx" required>
                </fieldset>
              </div>
              <div class="col-lg-6">
                <fieldset>
                    <label for="email" class="form-label">Email</label>
                    <input type="email" name="email" class="email" placeholder="xxx@email.com" required>
                </fieldset>
              </div>
              <div class="col-lg-6">
                  <fieldset>
                      <label for="kategori" class="form-label">Kategori</label>
                      <select name="kategori" class="form-select" aria-label="Default select example" id="kategori">
                          <?php foreach($kategori as $k): ?>
                          <option value="<?php echo $k["id"]; ?>"><?php echo $k["nama"] . " (" . ($k["kapasitas_min"] == $k["kapasitas_max"] ? $k["kapasitas_min"]: $k["kapasitas_min"] . " - " . $k["kapasitas_max"]) . ")"; ?></option>
                          <?php endforeach; ?>
                      </select>
                  </fieldset>
              </div>
              <div class="col-lg-6">
                  <fieldset>
                      <label for="tamu" class="form-label">Jumlah Tamu</label>
                      <input type="number" name="tamu" id="tamu" value="1" required>
                  </fieldset>
              </div>
              <div class="col-lg-12">
                <fieldset>
                    <label for="tgl" class="form-label">Tanggal Check In</label>
                    <input type="date" name="tgl" id="tgl" required>
                </fieldset>
              </div>
              <div class="col-lg-12">
                  <fieldset>
                      <label for="unit" class="form-label">Pilih Unit</label>
                      <select name="unit" class="form-select" aria-label="Default select example" id="unit">
                      </select>
                  </fieldset>
              </div>
              <div class="col-lg-12">                        
                  <fieldset>
                       <input type="submit" class="main-button" style="background: #82a3ac; color: #fff" value="Buat Pesanan" />
                  </fieldset>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <p>Copyright © 2022. 5RIKANDI Company.
            <br>
            All rights reserved.</p>
        </div>
      </div>
    </div>
  </footer>


  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

  <script src="assets/js/isotope.min.js"></script>
  <script src="assets/js/owl-carousel.js"></script>
  <script src="assets/js/tabs.js"></script>
  <script src="assets/js/popup.js"></script>
  <script src="assets/js/custom.js"></script>
  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>
  <script src="vendor/select2/select2.full.min.js"></script>

  <script>
    (() => {
      var element = document.getElementById('osm-map');
      var map = L.map(element);
      L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
          attribution: 'Srikandi Resort'
      }).addTo(map);
      var target = L.latLng('-8.1528202', '110.6158277');
      map.setView(target, 12);
      L.marker(target).on("click", (e) => {
        open("https://www.google.co.id/maps/place/Pantai,+Tepus,+Kec.+Tepus,+Kabupaten+Gunung+Kidul,+Daerah+Istimewa+Yogyakarta/@-8.1528202,110.6158277,14z/data=!3m1!4b1!4m5!3m4!1s0x2e7bb99df2a786cd:0xac232c33c942e20c!8m2!3d-8.1561723!4d110.6317907", "_blank")
      }).addTo(map);
    })()
  </script>

  <script>
    $(document).ready(() => {
      var $kategori = $("#kategori")

      function units(){
        $.post("api.php", {
          kategori: $kategori.val(),
          tamu: $("#tamu").val(),
          tgl: $("#tgl").val(),
        }, (data) => {
          $("#unit").html("")
          data.forEach((v, i, arr) => {
            $("#unit").append($("<option value=\"" + v.id + "\">" + v.unit + "</option>"))
          })
        }, "json")
      }

      units()

      $kategori.on("change", (e) => {
        g = {
        <?php foreach($kategori as $g): ?>
            "<?php echo $g["id"]; ?>" : {
                min : <?php echo $g["kapasitas_min"]; ?>,
                max : <?php echo $g["kapasitas_max"]; ?>,
            },
        <?php endforeach; ?>
        }
        
        kap = g[e.target.value]
        $("#tamu").attr("min", kap.min)
        $("#tamu").attr("max", kap.max)
        $("#tamu").val(kap.min)

        units()
      })

      $("#tgl").on("change", (e) => {
        units()
      })

      $("#tamu").on("change", (e) => {
        units()
      })

    })
  </script>

  </body>

</html>
