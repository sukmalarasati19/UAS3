<?php

include_once("../includes/login_check.php");
include_once("../includes/galeri.php");
$page = 0;
if(isset($_GET["p"])) $page = (int)$_GET["p"];
$offset = $page * $galeri_limit;

if(isset($_FILES["gambar"])){
    $gambar = $_FILES["gambar"];
    galeri_create_by_file($gambar);
}

$g = galeri($galeri_limit, $offset);
?>

<h2>Galeri</h2>
<hr/>
<p><a href="index.php">Menu</a></p>
<p><a href="logout.php">Keluar</a></p>

<?php foreach($g as $i): ?>
<img width="100" height="100" src="<?php echo $i["url"]; ?>" />
<?php endforeach; ?>

<form method="POST" enctype="multipart/form-data">
    <input type="file" name="gambar" accept="image/*" />
    <button type="submit">Upload</button>
</form>