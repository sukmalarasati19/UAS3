<?php
session_start();

if(isset($_SESSION["admin_id"])){
    header("Location: index.php");
    exit(0);
}


include_once("../includes/admin.php");
if(isset($_POST["nama"]) && isset($_POST["password"])){
    $ver = admin_verifikasi($_POST["nama"], $_POST["password"]);
    if ($ver){
        $_SESSION["admin_id"] = $ver["id"];
        if (isset($_SESSION["gagal_login"]))
            unset($_SESSION["gagal_login"]);

        header("Location: index.php");
        exit(0);
    }
    else
        $_SESSION["gagal_login"] = true;
}
?>

<h2>Login</h2>
<hr/>
<form method="POST">
    <?php if(isset($_SESSION["gagal_login"])): ?>
        <p style="color: #a00">Gagal Login</p>
    <?php endif; ?>
    <input placeholder="Nama" type="text" name="nama" /><br/>
    <input placeholder="Password" type="password" name="password" /><br/>
    <input type="submit" value="Masuk" />
</form>