<?php

include_once("../includes/login_check.php");
include_once("../includes/item.php");
include_once("../includes/area.php");
include_once("../includes/kategori.php");
include_once("../includes/galeri.php");

if(isset($_POST["id"]) && isset($_POST["unit"]) && isset($_POST["area"]) && isset($_POST["kategori"]) && isset($_POST["gambar"]) && isset($_POST["kapasitas"]) && isset($_POST["harga"])){
    item_update($_POST["id"], $_POST["unit"], $_POST["area"], $_POST["kapasitas"], $_POST["harga"], $_POST["gambar"], $_POST["kategori"]);
}
else if(isset($_POST["unit"]) && isset($_POST["area"]) && isset($_POST["kategori"]) && isset($_POST["gambar"]) && isset($_POST["kapasitas"]) && isset($_POST["harga"])){
    item_create($_POST["unit"], $_POST["area"], $_POST["kapasitas"], $_POST["harga"], $_POST["gambar"], $_POST["kategori"]);
}
else if(isset($_POST["hapus"])){
    item_delete($_POST["hapus"]);
}

$areas = area(100, 0);
$galeri = galeri(100, 0);
$items = item(100, 0);
$kategoris = kategori(100, 0);
?>
<head>
    <link href="../vendor/select2/select2.min.css" rel="stylesheet" />
</head>

<h2>Item</h2>
<hr/>
<p><a href="index.php">Menu</a></p>
<p><a href="logout.php">Keluar</a></p>

<table border="1">
    <?php foreach($items as $i => $item): ?>
    <tr>
        <td rowspan="5"><?php echo $i + 1; ?></td>
        <td rowspan="5"><img width="100" height="100" src="<?php echo $item["gambar"]; ?>" /></td>
        <th>Unit</th>
        <td><?php echo $item["unit"]; ?></td>
        <td rowspan="5">
            <form method="POST" style="display: inline;">
                <input type="hidden" name="edit" value="<?php echo $item["id"]; ?>"/>
                <input type="submit" value="Edit"/>
            </form>
            <form method="POST" style="display: inline;">
                <input type="hidden" name="hapus" value="<?php echo $item["id"]; ?>"/>
                <input type="submit" value="Hapus"/>
            </form>
        </td>
    </tr>
    <tr>
        <th>Area</th>
        <td><?php echo $item["area_nama"]; ?></td>
    </tr>
    <tr>
        <th>Kategori</th>
        <td><?php echo $item["kategori_nama"]; ?></td>
    </tr>
    <tr>
        <th>Kapasitas</th>
        <td><?php echo $item["kapasitas"]; ?></td>
    </tr>
    <tr>
        <th>Harga</th>
        <td>Rp <?php echo number_format($item["harga"],2,',','.'); ?></td>
    </tr>
    <?php endforeach; ?>
</table>

<form method="POST">
    <?php if(isset($_POST["edit"])): ?>
    <input type="hidden" name="id" value="<?php echo $_POST["edit"]; ?>"/>
    <?php endif; ?>
    <input placeholder="Unit" type="text" name="unit" id="unit" /><br/>
    <select style="width: 200px;" name="area" id="area">
        <?php foreach($areas as $a): ?>
        <option value="<?php echo $a["id"]; ?>"><?php echo $a["nama"]; ?></option>
        <?php endforeach; ?>
    </select><br/>
    <select style="width: 200px;" name="kategori" id="kategori">
        <?php foreach($kategoris as $k): ?>
        <option value="<?php echo $k["id"]; ?>"><?php echo $k["nama"] . " (" . ($k["kapasitas_min"] == $k["kapasitas_max"] ? $k["kapasitas_min"]: $k["kapasitas_min"] . " - " . $k["kapasitas_max"]) . ")"; ?></option>
        <?php endforeach; ?>
    </select><br/>
    <select style="width: 200px;" name="gambar" id="gambar">
        <?php foreach($galeri as $g): ?>
        <option value="<?php echo $g["id"]; ?>"><?php echo $g["url"]; ?></option>
        <?php endforeach; ?>
    </select><br/>
    <input placeholder="Kapasitas" type="number" id="kapasitas" name="kapasitas" max="<?php echo $kategoris[0]["kapasitas_max"]; ?>" min="<?php echo $kategoris[0]["kapasitas_min"]; ?>" value="<?php echo $kategoris[0]["kapasitas_min"]; ?>"/><br/>
    <input placeholder="Harga" type="number" name="harga" min="1000000" value="1000000" id="harga" /><br/>
    <input type="submit" value="Simpan" />
</form>

<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/select2/select2.full.min.js"></script>

<script>
$(document).ready(function() {
    function area(state){
        if (!state.id) {
            return state.text;
        }
        g = {
        <?php foreach($areas as $g): ?>
            "<?php echo $g["id"]; ?>" : ["<?php echo $g["nama"]; ?>", "<?php echo $g["gambar"]; ?>"],
        <?php endforeach; ?>
        }

        return $("<span><img height=\"100\" width=\"100\" src=\"" + g[state.element.value][1] + "\"/>" + g[state.element.value][0] + "</span>")
    }

    function gambar(state){
        if (!state.id) {
            return state.text;
        }
        g = {
        <?php foreach($galeri as $g): ?>
            "<?php echo $g["id"]; ?>" : "<?php echo $g["url"]; ?>",
        <?php endforeach; ?>
        }

        return $("<img height=\"100\" width=\"100\" src=\"" + g[state.element.value] + "\"/>")
    }

    function kategori(state){
        if (!state.id) {
            return state.text;
        }
        g = {
        <?php foreach($kategoris as $g): ?>
            "<?php echo $g["id"]; ?>" : "<?php echo $g["nama"] . " (" . ($g["kapasitas_min"] == $g["kapasitas_max"] ? $g["kapasitas_min"]: $g["kapasitas_min"] . " - " . $g["kapasitas_max"]) . ")"; ?>",
        <?php endforeach; ?>
        }

        return g[state.element.value]
    }

    var $gambar = $("#gambar").select2({
        templateResult:gambar
    });

    var $area = $("#area").select2({
        templateResult:area
    });

    var $kategori = $("#kategori").select2({templateResult:kategori})
    $kategori.on("change", (e) => {
        g = {
        <?php foreach($kategoris as $g): ?>
            "<?php echo $g["id"]; ?>" : {
                min : <?php echo $g["kapasitas_min"]; ?>,
                max : <?php echo $g["kapasitas_max"]; ?>,
            },
        <?php endforeach; ?>
        }
        kap = g[e.target.value]
        $("#kapasitas").attr("min", kap.min)
        $("#kapasitas").attr("max", kap.max)
        $("#kapasitas").val(kap.min)
    });

    
    <?php if(isset($_POST["edit"])): ?>
    <?php $edits = item_get($_POST["edit"]); ?>
    $("#unit").val("<?php echo $edits["unit"]; ?>");
    $("#area").val("<?php echo $edits["area_id"]; ?>");
    $area.trigger('change');
    $("#kategori").val("<?php echo $edits["kategori_id"]; ?>")
    $kategori.trigger('change')
    $("#kapasitas").attr("min", "<?php echo $edits["kategori_kapasitas_min"]; ?>")
    $("#kapasitas").attr("max", "<?php echo $edits["kategori_kapasitas_max"]; ?>")
    $("#kapasitas").val("<?php echo $edits["kapasitas"]; ?>")
    $("#gambar").val("<?php echo $edits["galeri_id"]; ?>")
    $gambar.trigger('change')
    $("#harga").val("<?php echo $edits["harga"]; ?>")
    <?php endif; ?>
})
</script>