<!DOCTYPE html>
<?php

include_once("../includes/login_check.php");
include_once("../includes/area.php");
include_once("../includes/galeri.php");

if(isset($_POST["nama"]) && isset($_POST["gambar"]) && isset($_POST["deskripsi"])){
    area_create($_POST["nama"], $_POST["gambar"], $_POST["deskripsi"]);
}

$areas = area(100, 0);
$galeri = galeri(100, 0);
?>
<html>
    <head>
        <link href="../vendor/select2/select2.min.css" rel="stylesheet" />
    </head>

    <body>
<h2>Area</h2>
<hr/>
<p><a href="index.php">Menu</a></p>
<p><a href="logout.php">Keluar</a></p>

<table border="1">
    <?php foreach($areas as $i => $a): ?>
    <tr>
        <td rowspan="2"><?php echo $i + 1; ?></td>
        <td rowspan="2"><img width="100" height="100" src="<?php echo $a["gambar"]; ?>" /></td>
        <th>Nama</th>
        <td><?php echo $a["nama"]; ?></td>
    </tr>
    <tr>
        <th>Deskripsi</th>
        <td><?php echo $a["deskripsi"]; ?></td>
    </tr>
    <?php endforeach; ?>
</table>
<form method="POST">
    <input placeholder="Nama" type="text" name="nama" /><br/>
    <select name="gambar" id="gambar">
        <?php foreach($galeri as $g): ?>
        <option value="<?php echo $g["id"]; ?>"><?php echo $g["url"]; ?></option>
        <?php endforeach; ?>
    </select><br/>
    <textarea id="deskripsi" name="deskripsi"></textarea>
    <input type="submit" value="Simpan" />
</form>

<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/ckeditor5/build/ckeditor.js"></script>
<script src="../vendor/select2/select2.full.min.js"></script>
<script>
$(document).ready(function() {
    ClassicEditor.create( document.querySelector( '#deskripsi' )).catch( error => {
        console.error( error );
    });

    function gambar(state){
        if (!state.id) {
            return state.text;
        }
        console.log(state)
        g = {
        <?php foreach($galeri as $g): ?>
            "<?php echo $g["id"]; ?>" : "<?php echo $g["url"]; ?>",
        <?php endforeach; ?>
        }

        return $("<img height=\"100\" width=\"100\" src=\"" + g[state.element.value] + "\"/>")
    }

    $("#gambar").select2({
        templateResult:gambar
    });
})
</script>
</body>
</html>