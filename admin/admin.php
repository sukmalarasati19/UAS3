<?php

include_once("../includes/login_check.php");
include_once("../includes/admin.php");

if (isset($_POST["nama"]) && isset($_POST["password"])) {
    try {
        $ok = admin_create($_POST["nama"], $_POST["password"]);
    }
    catch (Exception $e){}
}

if (isset($_POST["hapus"]))
    try {
        admin_delete($_POST["hapus"]);
    }
    catch (Exception $e){}

$admins = admin(10, 0);
?>

<h2>Admin</h2>
<hr/>
<p><a href="index.php">Menu</a></p>
<p><a href="logout.php">Keluar</a></p>

<table border="1">
    <tr>
        <th>No.</th>
        <th>Nama</th>
        <th>Aksi</th>
    </tr>
    <?php foreach($admins as $i => $a): ?>
    <tr>
        <td><?php echo $i + 1; ?></td>
        <td><?php echo $a["nama"]; ?></td>
        <td>
            <?php if($a["id"] != $admin_id): ?>
            <form method="POST" style="display: inline;">
                <input placeholder="Nama" type="hidden" name="hapus" value="<?php echo $a["id"]; ?>"/>
                <input type="submit" value="Hapus" />
            </form>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="3">
        <form method="POST">
            <input placeholder="Nama" type="text" name="nama" /><br/>
            <input placeholder="Password" type="text" name="password" /><br/>
            <input type="submit" value="Simpan" />
        </form>
        </td>
    </tr>
</table>